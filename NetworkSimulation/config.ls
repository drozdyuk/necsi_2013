exports.config =
  # See docs at https://github.com/brunch/brunch/blob/master/docs/config.md
  
  modules:
    definition: false # for my scripts - no need to wrap as livescript wraps already
    wrapper: false    # for all sripts - including vendor
    addSourceURLs: true

  stylesheets:
     joinTo:
       'css/app.css': /^(app|vendor)/

  files:    
    javascripts:
      joinTo:
        'js/vendor.js': /^vendor/
        'js/app.js': /^app/
      order:
        before: 
          'vendor/angular/angular.min.js'
          'vendor/angular/angular-resource.js'
          

  # Enable or disable minifying of result js / css files.
  # minify: true