basePath = '../';

files = [
  JASMINE,
  JASMINE_ADAPTER,
  'public/js/vendor.js',
  'public/js/app.js',
  'test/lib/angular/angular-mocks.js',
  'test/unit/**/*.ls'
];

autoWatch = true;

browsers = ['Chrome'];

junitReporter = {
  outputFile: 'test_out/unit.xml',
  suite: 'unit'
};

preprocessors = {
  '**/*.ls' : 'live'
}


