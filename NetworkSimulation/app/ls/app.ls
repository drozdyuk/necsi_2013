angular.module('simulation', 
    ['prelude',
     'simulation.network',
     'simulation.simulator',
     'simulation.controllers',
     'simulation.directives'])
.config(
['$routeProvider', ($routeProvider) -> 
    $routeProvider.when('/simulation', {
         templateUrl: 'partials/simulation.html', 
         controller: 'Simulation'});
    $routeProvider.when('/view2', {
         templateUrl: 'partials/partial2.html', 
         controller: 'MyCtrl2'});
    $routeProvider.otherwise({redirectTo: '/simulation'});
]);


