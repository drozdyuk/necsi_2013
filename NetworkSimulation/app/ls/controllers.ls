module = angular.module('simulation.controllers', 
    ['simulation.network'])

module.controller('Simulation', !($scope, $timeout, simulation)->
    simulator = simulation.Simulator
    $scope.data = simulator.graph

    $scope.grow = !-> simulator.grow!

    $scope.kaboom = !->
        grow_by(250)
    $scope.fast_grow = !->
        grow_by(50)
    
    grow_by = !(num) ->
        i = 0
        while i < num
            i += 1
            simulator.grow!
    
    var prom

    $scope.start = !->
        simulator.grow!
        prom := $timeout($scope.start, 1000)

    $scope.stop = !->        
        $timeout.cancel(prom)


)

module.controller('MyCtrl2', [!->]);