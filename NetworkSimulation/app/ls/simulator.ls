module = angular.module('simulation.simulator', 
    ['simulation.network', 'prelude'])
module.factory('simulation', (network, prelude) -> {
  Simulator : new Simulation(network, prelude)
})

class Simulation

    !(network, prelude)->
        # How old the graph is
        @graph_age = 1
        @prelude = prelude
        @curr_node = -1
        @graph = network.Graph!        

    # Generate uniform dist random num betw 0 and 1 
    generate_fitness :  ->        
        r = Math.random!
        plarge = Math.random!
        if r > 0.5 and plarge < 0.9 then
            r := (1-r)/2
        
        return r
        

    # Grow the network 1 node at a time
    grow : !->
        Math.seedrandom!
        node = @generate_node!
        fitness = @generate_fitness!

        if @graph.nodes.length == 0 then
            @graph.nodes.push(node)
        else
            dest = @find_random_attachment(node)
            if @graph.get_node_value.invalidate then
                # Try not to connect to an invalid node
                dest := @find_random_attachment(node)
            @graph.add(node, dest)

            # Generate a second edge!
            if Math.random! > 0.001 then
                extraEdge = @find_random_attachment(node)
                if extraEdge != node  then
                    @graph.add(node, extraEdge)

                    # increase the fitness
                    fitness := fitness * 2
                    if fitness >= 1 then fitness = 1.0
        
        @graph_age := @graph_age + 1

        node_metadata = {id:node, age: 0, fitness: fitness}
        
        @graph.set_node_value(node, node_metadata)

        # Increase the age of the older nodes
        @age_nodes(@graph.nodes, @graph_age)

        # Reduce target-node's fitness randomly
        @adjust_fitness(@graph.nodes, 0.2, 0.0005)



        
    # @param threshold - do not adjust nodes below this level, already too low
    #                    or higher than (1-threshold) - already too high
    adjust_fitness: !(nodes, threshold, prob) ->
        for node in nodes
            @invalidate_node(node, threshold, prob)   
            @validate_node(node, 1-threshold, prob)         

    validate_node: !(node, threshold, prob) ->        
        node_value = @graph.get_node_value(node)
        # Once node is invalidate, there is no way back...
        if node_value.invalidated then return
        if node_value.fitness < threshold and Math.random! < prob then
            node_value.fitness = node_value.fitness * 1.5
            if node_value.fitness >= 1.0 then node_value.fitness = 1.0
            

    # Set some of the most prominant nodes to be obsolete
    invalidate_node: !(node, threshold, prob) ->
        node_value = @graph.get_node_value(node)
        if node_value.fitness > threshold and Math.random! < prob then
            node_value.fitness = node_value.fitness/2
            
            node_value.invalidated = true

    # Age all node's metadata
    # normalized by number of nodes
    age_nodes: (nodes, curr_age) ->
        for node in nodes
            node_value = @graph.get_node_value(node)
            node_value.age += 1
            node_value.curr_age = curr_age

    # Generate a new node
    generate_node : ->
        @curr_node++
        @curr_node

    # Find nth closest node with cumulative probility closes to p 
    # E.g. if 
    # p = 0.3
    # pc = [ {node: 1, cumulative: 0.1},
    #        {node: 3, cumulative: 0.4}
    #      ]
    # Function would return {node: 3, cumulative: 0.4}
    # @param pc Cumulative probabilities for nodes, sorted in increasing order
    # @param p Probability to search for
    find_closest: (pc, p) ->
        if pc.length == 0 then throw Error("No nodes present")
        if pc.length == 1 then return pc[0]

        ix = @binary_search(pc, p, 0, pc.length-1, @p_comparator)
        return pc[ix]

    p_comparator: (element, list_element) ->
        if element < list_element.cumulative
            return -1
        else if element > list_element.cumulative
            return 1
        else
            return 0

    # Binary search for p, starting at index i
    # returns index at which the required element resides 
    binary_search: (list, element, iFrom, iTo, compare) ->
        
        if iFrom > iTo then throw Error("Invalid binary search indicies.")
        if iFrom == iTo then return iFrom

        # Pick a pivot
        pivot = iFrom + Math.ceil((iTo - iFrom)/2) 

        if compare(element, list[pivot]) < 0 then
            # search left side
            @binary_search(list, element, iFrom, pivot - 1, compare)
        else
            # search right side
            @binary_search(list, element, pivot, iTo, compare)

    
    # Returns a node from the graph to which
    # to attach this node
    find_random_attachment: (node) ->
        Math.seedrandom!
        p = Math.random!
        ps = @calculate_attach_probabilities(@graph)
        
        node  = @find_closest(ps, p).node
        
        #if(@graph.nodes.length==0)
        #    rand_index = 0
        #else
        #    rand_index = Math.floor((Math.random() *@graph.nodes.length))
        
        #TODO: remporarily always attach tofirst node
        #return @graph.nodes[rand_index]
        return node
        
    # Calculate how much "weight" this node carries
    # This weight/total_weight will give the probably of attachment to this node.    
    node_weight: (node) ->
        node_value = @graph.get_node_value(node)

        IN_DEG = @graph.in_degree(node) + 1

        #AGE = 1/(node_value.age/node_value.curr_age) * IN_DEG

        #RANDOM = 1/@graph.nodes.length
        #STRONG_PREF = ( @graph.in_degree(node)+ 1)/@graph.nodes.length
        #FIT = (1 / (1 - node_value.fitness)) * AGE
        
        #age_score = node_value.curr_age
        #in_deg_score = IN_DEG
        #fit = (1 - 1/(node_value.fitness + 1))
        #fit_score = fit *  1/(age_score + 1) * 1 / (IN_DEG + 1)

        r = Math.log(IN_DEG + 1) * 1/(node_value.age/node_value.curr_age) * (1 - 1/( Math.sqrt(node_value.fitness) + 1 ))
        return r
    

    normalization: ->
        norm = 0.0
        for node in @graph.nodes
                norm += @node_weight(node)
        return norm
        #return @graph.nodes.length + @graph.num_edges

    # Calculate cumulative probabilities of attachment 
    # for each node and return a list sorted by cumulative
    # probabilities. E.g.:
    # [ 
    #   {node: 1, cumulative: 0.01},
    #   {node: 3, cumulative: 0.04}
    # ]
    calculate_attach_probabilities: (graph)->
        # Sanity checks:
        # - If no nodes - nothing to connect to
        if graph.nodes.length == 0 then return []
        # - If only one node, connect to it!
        if graph.nodes.length == 1 then return [
            {
                node : graph.nodes[0],
                cumulative : 1.0
            }
        ]
        degs = []
        cumulative = 0.0
        num_edges = graph.num_edges
        
        for node in graph.nodes         
            
            p_attach = @node_weight(node)/@normalization!
            cumulative += p_attach
           
            info = {node : node , cumulative : cumulative }
            degs.push(info)

        # Sort the list by cumulative
        f = (x, y) -> 
                    | x.cumulative > y.cumulative => 1    
                    | x.cumulative < y.cumulative => -1
                    | otherwise => 0
        
        return  @prelude.sort-with f, degs 