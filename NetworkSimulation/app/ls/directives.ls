module = angular.module('simulation.directives', [])

module.directive('ghVisualization', -> 

  width = 1200
  height = 900
  color = d3.interpolateRgb("\#f77", "\#77f")
  radius = 5

  return {
    restrict: 'E',
    scope: {
      val: '=',
      grouped: '='
    },
    link: !(scope, element, attrs) ->
      nodes = []
      links = []
      fitness_scale = d3.scale.linear!
      .range([4,12])
      age_color_scale = d3.scale.linear!
      .interpolate(d3.interpolateRgb)
      # newest E0E0E0 -  black is the older 000000
      .range(["\#FF3366", "\#002EB8"])
      #.range(d3.interpolateRgb('rgb(250,250,250)', 'rgb(0,0,0)'))

      vis = d3.select(element[0])
            .append('svg')
            .attr('width', width)
            .attr('height', height)

      node = vis.selectAll('.node')
      link = vis.selectAll('.link')
      
      tick = !->
        node.attr('cx', (d) -> d.x )
        node.attr('cy', (d) -> d.y )
        node.attr('r', (d) -> fitness_scale(d.fitness))
        node.style("fill", (d) -> 
          if d.invalidated then '\#808080'
          else age_color_scale(d.age/d.curr_age)
        )

        link.attr('x1', (d) -> d.source.x)
        link.attr('x2', (d) -> d.target.x)
        link.attr('y1', (d) -> d.source.y)
        link.attr('y2', (d) -> d.target.y)

      force = d3.layout.force!
      .nodes(nodes)
      .alpha(0.01)      
      .links(links)
      .size([width, height])
      .on("tick", tick)

      
      # LOGIC
      start = !->
        
        link := link.data(force.links!, (d) -> (d.source.id + "-" + d.target.id))
  
        link.enter!.insert('line', '.node').attr('class', 'link')
        link.exit!.remove!

        node := node.data(force.nodes!, (d) -> d.id)
        node.enter!.append('circle')
        .attr('class', (d) -> "node " + d.id)
        .attr('r', (d) -> fitness_scale(d.fitness))
        node.style("fill", (d) -> 
          col = age_color_scale(d.age/d.curr_age); 

          col)


        node.exit!.remove!
        force.start!
      
      scope.$watch('val', !(newVal, oldVal) ->        
        if newVal is void then           
          return 
        
        for source in newVal.nodes

          # Is this a new node?
          sourceValue = newVal.get_node_value(source)
          if sourceValue not in nodes then

            # New node - add it!
            nodes.push(sourceValue)

            # Connect the links
            for target in newVal.out_nodes(source)
              targetValue = newVal.get_node_value(target)
              links.push({source:sourceValue, target:targetValue})
          
            
        start!
      , true)
  }
)

     
      