module = angular.module('simulation.network', [])
module.factory('network', ->
   {
      Graph : -> new AddOnlyNetwork
   })

# Network that allows only addition of nodes and edges
# and not their removal.
class AddOnlyNetwork 
      # Constructor
      ->
         # Stores forward edges
         @_matrix = {}
         # Stores backward edges
         @_matrix_back = {}
         @nodes =  []
         @node_values = {}
         @edge_values = {}
         @num_edges = 0

      # Add an edge between two nodes
      # If the node does not exist it is added
      add: !(source, dest) ->
         if source not of @_matrix then 
            @_matrix[source] = []
         if dest not in @_matrix[source] then
            @_matrix[source].push(dest)
      
         # Also add the edge to backward matrix
         if dest not of @_matrix_back then
            @_matrix_back[dest] = []
         if source not in @_matrix_back[dest] then
            @_matrix_back[dest].push(source)

         # Cache nodes for fast access
         if source not in @nodes then @nodes.push(source)
         if dest not in @nodes then @nodes.push(dest)

         # Add edge count
         @num_edges++
         
      out_degree: (node) -> @out_nodes(node).length
      in_degree: (node) -> @in_nodes(node).length

      # Neighbors
      out_nodes: (node) ->
            if node not of @_matrix then []
            else @_matrix[node]

      in_nodes: (node) ->
            if node not of @_matrix_back then []
            else @_matrix_back[node]

      adjacent: (source, target) ->
            if source not of @_matrix then false
            else target in @_matrix[source]

      set_edge_value: !(source, dest, value) ->
            # If there is no edge - don't set the value
            if not @adjacent(source, dest) then return
            if source not of @edge_values then @edge_values[source] = {}
            @edge_values[source][dest] = value

      get_edge_value: (source, dest) ->
            if source not of @edge_values then {}
            else if dest not of @edge_values[source] then {}
            else @edge_values[source][dest]

      # @param value - dictionary with propeties
      set_node_value: !(node, value) ->
            # If node is not part of the graph - ignore it
            @node_values[node] := value
            

      # Node value is a dictionary with values
      # @return Dictionary associated with a particular node
      get_node_value: (node) ->
            if node not of @node_values then {}
            else @node_values[node]


      delete: (source, target) ->
            # To implement delete need to update all
            # storage structures. Not required for now.
            throw new Error("Deletion not supported.")
