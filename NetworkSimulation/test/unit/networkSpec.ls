describe('service', !->
  beforeEach(module('simulation.network'))

  describe('network',  !(void)->
    it('node should be added', !->
      inject !(network) ->

        n = network.Graph!


        expect(n.num_edges).toBe(0)
        n.add('A', 'B');
        expect(n.nodes[0]).toEqual('A');
        expect(n.num_edges).toBe(1)
        n.add('A', 'C');
        expect(n.num_edges).toBe(2)
        # A --> B
        #   \-> C
        expect('A' in n.nodes and 'B' in n.nodes and 'C' in n.nodes)
        .toBe(true)
        
        expect('B' in n.out_nodes('A') and 'C' in n.out_nodes('A'))
        .toBe(true)
        expect('A' in n.in_nodes('C') and 'A' in n.in_nodes('B'))
        .toBe(true)

        expect(n.in_nodes('A')).toEqual([])
        
        expect(n.in_degree('A')).toBe(0)
        expect(n.out_degree('A')).toBe(2)
        expect(n.in_degree('B')).toBe(1)
        expect(n.out_degree('B')).toBe(0)

        expect(n.adjacent('A', 'C')).toBe(true)
        expect(n.adjacent('A', 'X')).toBe(false)

        n.set_node_value('A', {'fitness': 1})
        expect(n.get_node_value('A').fitness).toEqual(1)

        n.set_edge_value('A', 'B', {'weight': 20})
        expect(n.get_edge_value('A', 'B').weight).toEqual(20)

        # Make sure an instance is not shared
        n2 = new network.Graph
        expect(n2.nodes.length).toEqual(0)

    )
  )

  
)
