describe('service', !->
  beforeEach(module('simulation.simulator'))

  describe('simulator',  !(void)->
    it('simulation should add 1 node at a time', !->
      inject !(simulation) ->

        s = simulation.Simulator;
        expect(s.graph.nodes.length).toEqual(0)
        s.grow!
        expect(s.graph.nodes.length).toEqual(1)
        s.grow!
        s.grow!
        expect(s.graph.nodes.length).toEqual(3)
        # Test that the random attachment
        # returns a node that is part of the graph
        n = s.generate_node!
        dest = s.find_random_attachment(n)
        expect(dest in s.graph.nodes).toBe(true)

        
        
    )
  )

  
)
